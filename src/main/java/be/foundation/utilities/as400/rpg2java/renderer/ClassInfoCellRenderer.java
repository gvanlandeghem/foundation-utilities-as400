/*
 * Created on 1-nov-2005
 *
 */
package be.foundation.utilities.as400.rpg2java.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * @author     : Geert Van Landeghem
 * Copyright : Foundation.be
 */
public class ClassInfoCellRenderer extends JLabel implements TableCellRenderer {

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setOpaque(true);
        String type = table.getValueAt(row,3).toString();
        if (type.equals("constructor")) {
            this.setBackground(Color.blue);
            this.setForeground(Color.white);
        } else if (type.equals("method"))  {
            this.setBackground(Color.white);
            this.setForeground(Color.black);
        } else if (type.equals("field"))  {
            this.setBackground(Color.green);
            this.setForeground(Color.white);
        }
        setText(value.toString());
        return this;
    }
}
