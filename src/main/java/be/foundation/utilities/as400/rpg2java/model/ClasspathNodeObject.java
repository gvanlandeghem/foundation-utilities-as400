/*
 * Created on 1-nov-2005
 *
 */
package be.foundation.utilities.as400.rpg2java.model;

/**
 * @author    : Geert Van Landeghem
 * Copyright : Foundation.be
 */
public class ClasspathNodeObject {
    private String str;
    private ClasspathNodeObjectType type;
    
    public ClasspathNodeObject(String str, ClasspathNodeObjectType type) {
        this.str = str;
        this.type = type;
    }

    public String toString() {
        return str;
    }
    
    public ClasspathNodeObjectType getType() {
        return type;
    }
}
