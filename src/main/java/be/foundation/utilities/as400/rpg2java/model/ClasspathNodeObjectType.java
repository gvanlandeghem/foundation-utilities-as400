/*
 * Created on 1-nov-2005
 *
 */
package be.foundation.utilities.as400.rpg2java.model;

/**
 * @Author    : Geert Van Landeghem
 * Copyright : Foundation.be
 */
public class ClasspathNodeObjectType {
    public static final ClasspathNodeObjectType TYPE_JAR = new ClasspathNodeObjectType(1);
    public static final ClasspathNodeObjectType TYPE_PACKAGE = new ClasspathNodeObjectType(2);
    public static final ClasspathNodeObjectType TYPE_JAVA = new ClasspathNodeObjectType(3);
    public static final ClasspathNodeObjectType TYPE_UNDEFINED = new ClasspathNodeObjectType(4);
    
    private int type;

    private ClasspathNodeObjectType(int type) {
        this.type = type;
    }

    public int getType(){
        return type;
    }

}
