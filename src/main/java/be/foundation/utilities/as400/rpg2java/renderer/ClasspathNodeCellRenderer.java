/*
 * Created on 1-nov-2005
 *
 */
package be.foundation.utilities.as400.rpg2java.renderer;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

import be.foundation.utilities.as400.rpg2java.model.ClasspathNodeObject;
import be.foundation.utilities.as400.rpg2java.model.ClasspathNodeObjectType;

/**
 * @author    : Geert Van Landeghem
 * Copyright : Foundation.be
 */
public class ClasspathNodeCellRenderer extends JLabel implements TreeCellRenderer {

    private static final ImageIcon IMAGE_JAR;
    private static final ImageIcon IMAGE_PACKAGE;
    private static final ImageIcon IMAGE_JAVA;
     
    static {
        IMAGE_JAR = createImageIcon("/be/foundation/utilities/as400/rpg2java/renderer/images/jar.gif", "jar");
        IMAGE_PACKAGE = createImageIcon("/be/foundation/utilities/as400/rpg2java/renderer/images/package.gif", "package");
        IMAGE_JAVA = createImageIcon("/be/foundation/utilities/as400/rpg2java/renderer/images/java.gif", "java");
        //IMAGE_PACKAGE = new ImageIcon(cl.getResource("/be/foundation/utilities/as400/rpg2java/package.gif"));
        //IMAGE_JAVA = new ImageIcon(cl.getResource("/be/foundation/utilities/as400/rpg2java/java.gif"));
        System.out.println("loaded images!!!");
    }
    
    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path,
                                               String description) {
        java.net.URL imgURL = ClasspathNodeCellRenderer.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode)value;
        ClasspathNodeObject nodeObject = (ClasspathNodeObject)dmtn.getUserObject();
        if (nodeObject.getType().equals(ClasspathNodeObjectType.TYPE_JAR)) {
            setIcon(IMAGE_JAR);
        } else if (nodeObject.getType().equals(ClasspathNodeObjectType.TYPE_PACKAGE)) {
            setIcon(IMAGE_PACKAGE);
        } else if (nodeObject.getType().equals(ClasspathNodeObjectType.TYPE_JAVA)) {
            setIcon(IMAGE_JAVA);
        }
        setText(nodeObject.toString());
        return this;
    }
}
